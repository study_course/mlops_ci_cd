import pandas as pd
import click


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path_train", type=click.Path())
@click.argument("output_path_val", type=click.Path())
@click.argument("output_path_test", type=click.Path())
def train_test_split(
    input_path: str, output_path_train: str, output_path_val: str, output_path_test: str
):
    """
    This function separate dataset for 3 set - train, valid, test
    :param input_path:Path to full dataset
    :param output_path_train: Path to save train dataset
    :param output_path_val: Path to save valid dataset
    :param output_path_test: Path to save test dataset
    :return:
    """
    df = pd.read_csv(input_path)
    group_ts = df.groupby(["article_id"] + ["Date"])["Qty"].sum().unstack(fill_value=0)
    data_sales_train = group_ts.iloc[:, :-200]
    data_sales_valid = group_ts.iloc[:, -200:-100]
    data_sales_test = group_ts.iloc[:, -100:]
    data_sales_train.to_csv(output_path_train)
    data_sales_valid.to_csv(output_path_val)
    data_sales_test.to_csv(output_path_test)


if __name__ == "__main__":
    train_test_split()
