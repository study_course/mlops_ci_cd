import pandas as pd
import click


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
def preparing_data(input_path: str, output_path: str):
    """
    This function is used to clean anomalies
    :param input_path: Path to full dataset
    :param output_path: Path to save dataset without anomalies
    :return:
    """
    df = pd.read_csv(input_path)
    avg_sales_pd = (
        df.groupby(["article_id"])["Qty"].mean().rename("mean_qty").reset_index()
    )
    data_with_avg = pd.merge(df, avg_sales_pd, on="article_id")
    filtered_data = data_with_avg.loc[
        data_with_avg["Qty"] <= 10 * data_with_avg["mean_qty"]
    ]
    filtered_data[["article_id", "Date", "Qty"]].to_csv(output_path, index=False)


if __name__ == "__main__":
    preparing_data()
